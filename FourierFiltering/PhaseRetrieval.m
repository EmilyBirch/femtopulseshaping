classdef PhaseRetrieval
   methods(Static) 
       
       
       % The Target is a desired temporal profile,
       % The source is the spectral profile of the actual pulse, assumed to
       % have a random phase
       function [RetrievedPhase] = GerchbergSaxton(Etarget_t,Esource_w)
           % normalise
           Etarget_t = abs(Etarget_t) / max(Etarget_t);
           Esource_w = Esource_w / max(Esource_w);
           
           N = length(Esource_w);
           Ep_nnextw = Esource_w;

           n = 0;
           while n < 500
               %____________________________________
               % nth iteration
               
               E_nw = Ep_nnextw;
               
               % E_n(t) = IFFT(E_n(w))
               E_nt = Basics.SpectralToTemporal(E_nw); % Get Source to time domain;               
               
               % E'_n(t) = E_target(t) . e^(i phase(E_n(t)))
               Ep_nt = abs(Etarget_t) .* (exp(1i * angle(E_nt)));
               
               % E'_n(w) = FFT(E'_n(t))
               Ep_nw = Basics.TemporalToSpectral(Ep_nt);
              
               % E_n+1(w) = Esource(w) . e^(i phase(E'_n(w)))
               Ep_nnextw = abs(Esource_w) .* (exp(1i .* angle(Ep_nw)));
               n = n + 1;
               %____________________________________
                              % nth iteration
%                B = abs(Etarget_t) .* (exp(1i .* angle(A)));
%                C = fft(B)/N;
%                D = abs(Esource_w) .* (exp(1i .* angle(C)));
%                A = ifft(D);
%                               
%                n = n + 1;
               %____________________________________
           end
       

            RetrievedPhase = angle(Ep_nnextw);
           
           OutputE_t = Basics.SpectralToTemporal(Ep_nnextw);
           InputE_t = Basics.SpectralToTemporal(Esource_w); 
           
            % plot temperoal profiles
            figure()
            title("Plot of Optimised time domain");
            plot(abs(OutputE_t)/max(abs(OutputE_t))); hold on;
            plot(abs(InputE_t)/max(abs(InputE_t))); hold on;
            plot(abs(Etarget_t));
            legend("Optimised", "Unoptimised", "Target");
                 
            
            % plot frequency spectra
%             figure()
%             plot(abs(Ep_nnextw)/max(abs(Ep_nnextw))); hold on;
%             plot(abs(Esource_w)/max(abs(Esource_w)));
%             legend("Optimised", "Source");

       end
       
       function [ErrorMeasure] = GSError(Target,Source)
           ErrorMeasure = 20;
       end
   end
end