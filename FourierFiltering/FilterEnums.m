classdef FilterEnums
    enumeration
        None,
        Amplitude_Ramp,
        Phase_Polynomial,
        Amplitude_Gaussian,
        Amplitude_Box,
        Amplitude_Notch,
        Phase_Triangle,
        Phase_Ramp
    end
end

