classdef InputPulseFunctionEnum
    enumeration
        Gaussian,
        Sinc,
        Sech,
        SkewGaussian
    end
end

