classdef SpatialCalibration
	methods(Static)   
        function [m,c] = LinearSpatialFit(filename)
            curDir   = pwd;
            mainDir  = curDir;
            file = fullfile(mainDir, 'CalibrationData', filename)

            M = csvread(file);
            N = length(M);
            Frequency = 1:N;
            Position = 1:N;


            for i=1:N
               Frequency(i) = M(i,1);
               Position(i) = M(i,2);
            end          

            P = polyfit(Frequency,Position,1);
            m = P(1);
            c = P(2);
            
            fit = (Frequency * m) + c;            

        end
    end
end