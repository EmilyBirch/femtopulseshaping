function [] = FourierPlanePlotting(Constants, time_domain,X_domain,X_type,InputPulse,OutputPulse,SpectralAmplitudeFilter,SpectralPhaseFunction,cutOff)

    N = length(X_domain);
    t = time_domain;
    t_increments    = (2 * InputPulse.Start_time)/(N-1);  
    t_plot_lim = InputPulse.Start_time;
    
    
    if X_type == "Frequency"
        X = X_domain;
        X_plot_min = InputPulse.PulseFrequency - (InputPulse.PulseFrequency);
        X_plot_max = InputPulse.PulseFrequency + (InputPulse.PulseFrequency) ;
        
    elseif X_type == "Space"
        X = X_domain;
        X_plot_min = (InputPulse.PulseFrequency -(InputPulse.PulseFrequency))  * InputPulse.x_dispersion;
        X_plot_max = (InputPulse.PulseFrequency + (InputPulse.PulseFrequency))  * InputPulse.x_dispersion;
    end 
    
  
    figure('Name','Fourier Plane SpectralAmplitudeFilter', ...
        'NumberTitle','off', ...
        'Units','normalized', ...
        'Position', [0 0 0.95 0.95]);
    clf;
    sgtitle('Fourier Plane Pulse Shaping')
    
    % Plotting E(time_domain) before SpectralAmplitudeFilter
    subplot(3,3,1)
    plot(t, InputPulse.I_t);
    xlim([-t_plot_lim t_plot_lim]);
    xlabel('Time (fs)');
    ylabel('|E(t)|^2');
    title('|E(t)|^2 Before Filtering');
    
    % Plotting E(f) before SpectralAmplitudeFilter
    subplot(3,3,4)
    title('|E(f)|^2 Before Filtering')
    ylabel('|E(f)|^2');   
    plot(X,InputPulse.I_f/max(InputPulse.I_f));
    xlim([X_plot_min, X_plot_max]);
    
    if X_type == "Frequency"
        xlabel('Frequency (THz)');
    elseif X_type == "Space"
        xlabel('Space (m)');        
    end
    
    
    % Plotting E(X) 
    subplot(3,3,2)
    plot(X,InputPulse.I_f/max(InputPulse.I_f));
    ylabel('|E(X_\omega)|^2');
    title('|E(X_\omega)|^2 Before Filtering');
    xlim([X_plot_min, X_plot_max]);
    if X_type == "Frequency"
        xlabel('Frequency (THz)');
    elseif X_type == "Space"
        xlabel('Space (m)');        
    end

    % Plotting Spatial Filter
    subplot(3,3,5)
    yyaxis left;
    plot(X,SpectralAmplitudeFilter); hold on;
    plot(X,InputPulse.I_f/max(InputPulse.I_f),'--','Color',[0,0,0]);
    ylabel('G(X_\omega)');
    xlim([X_plot_min, X_plot_max]);
    if X_type == "Frequency"
        xlabel('Frequency (THz)');
    elseif X_type == "Space"
        xlabel('Space (m)');        
    end
    title('Spatial Amplitude Filter');
    
    yyaxis right;
    plot(X,SpectralPhaseFunction); 
    ylabel('p(X_\omega)');
    xlim([X_plot_min, X_plot_max]);
    if X_type == "Frequency"
        xlabel('Frequency (THz)');
    elseif X_type == "Space"
        xlabel('Space (m)');        
    end
    legend('Amplitude','Pulse','Phase')



    % Plotting E(f) after SpectralAmplitudeFilter
    subplot(3,3,6)
    plot(X,OutputPulse.I_f/max(OutputPulse.I_f));
    xlim([X_plot_min, X_plot_max]);
    title('|E(f)|^2 After Filtering');
    if X_type == "Frequency"
        xlabel('Frequency (THz)');
    elseif X_type == "Space"
        xlabel('Space (m)');        
    end
    ylabel('|E(f)|^2');

    % Plotting E(time_domain) after SpectralAmplitudeFilter
    subplot(3,3,3)    
    plot(t, OutputPulse.I_t);
    xlim([-t_plot_lim t_plot_lim]);
    xlabel('Time (fs)');
    ylabel('|E(t)|^2');
    title('|E(t)|^2 After Filtering');
    
%__________________________________________________________________________
% Spectrogram Plot
    
    figure('Name','Fourier Plane Spectrogram', ...
            'NumberTitle','off', ...
            'Units','normalized', ...
            'Position', [0 0 0.70 0.70]);
    clf;
    sgtitle('Fourier Plane Pulse Shaping Spectrogram')
    
     a = (floor(log2(N)));
     b = a - 5;
     c = b - 3;

     t_plot_lim = InputPulse.Start_time
     t_increments
     
     spectrogram(OutputPulse.E_t,2^b,2^c,2^a,1/(t_increments),'yaxis');
     %xlim([-t_plot_lim+(InputPulse.Start_time), t_plot_lim+(InputPulse.Start_time)]);
     ylim([(InputPulse.PulseFrequency - (200 * Constants.tera))/(Constants.peta), ...
           (InputPulse.PulseFrequency + (200 * Constants.tera))/(Constants.peta)])
end

