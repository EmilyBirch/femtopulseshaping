curDir   = pwd;
mainDir  = curDir;
file = fullfile(mainDir, 'Data', 'HFPFspec.csv')

M = csvread(file);
N = length(M);
Frequency = 1:N;
Amplitude = 1:N;

const = Constants;

for i=1:N
   Frequency(i) = M(i,1);
   Amplitude(i) = M(i,2);
end

freq_domain= Frequency .* 1E15 ; % Convert to THz scale
freq_interval = sum(freq_domain) / N; 

time_domain = -(ceil(N/2)):floor((N-1)/2);
time_domain = (time_domain * N)/freq_interval;

            
InputPulse.E_f = Amplitude / max(Amplitude);

InputPulse.E_t = Basics.SpectralToTemporal(InputPulse.E_f);


figure()
plot(time_domain,abs(InputPulse.E_t))

InputPulse.PulseWidth = 2000;
InputPulse.PulseWidthRising = 100; % fs
InputPulse.PulseWidthFalling = 6000; % fs
InputPulse.PulseType = InputPulseFunctionEnum.SkewGaussian;
E_t_target = Basics.GenerateTemporalPulseEnvelope(const,InputPulse,time_domain);
[banana] = PhaseRetrieval.GerchbergSaxton(E_t_target, InputPulse.E_f);