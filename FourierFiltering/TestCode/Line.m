m = 1.9154e-17
S1_c = 0.01;
S2_c = 0.005;
FM_c = 0.02;
N = 100
f = (0:N-1) * (10^13);

S1_x = (f * m) + S1_c;
S2_x = (f * m) + S2_c;
FM_x = (f * m) + FM_c;

S1data = [f;S1_x];
S2data = [f;S2_x];
FMdata = [f;FM_x];

dlmwrite('CalibrationData/S1Calibration.csv',transpose(S1data),'precision',6);
dlmwrite('CalibrationData/S2Calibration.csv',transpose(S2data),'precision',6);
dlmwrite('CalibrationData/FMCalibration.csv',transpose(FMdata),'precision',6);