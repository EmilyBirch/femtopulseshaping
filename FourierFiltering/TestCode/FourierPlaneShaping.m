%__________________________________________________________________________
const = Constants;

%__________________________________________________________________________
% Define Hardware const

Hardware.f       = 30; % focal length (cm)
Hardware.a       = 1.67e-6; % grating period 
Hardware.theta_d = -0.65; % radians 

%__________________________________________________________________________
% Input Pulse Definition
InputPulse.PulseType        = InputPulseFunctionEnum.SkewGaussian;
InputPulse.PulseWidth       = 10 * const.femto; 	% fs
InputPulse.PulseWidthRising = 10 * const.femto;
InputPulse.PulseWidthFalling = 40 * const.femto;
InputPulse.PulseWavelength  = 800 * const.nano; % nm
InputPulse.PulseFrequency   = Basics.mToHz(const,InputPulse.PulseWavelength)
InputPulse.x_dispersion  =  ...
                  (((InputPulse.PulseWavelength)^2)*Hardware.f)/...
                  (2*pi*const.c*Hardware.a*cos(Hardware.theta_d));

%__________________________________________________________________________
N       = 10000;  % Number of samples in the simulation
cutOff  = ceil(N/2);
[InputPulse,t_increments,time_domain,freq_domain] = ...
         Basics.GenerateTimeDomain(const,InputPulse,N);  

%__________________________________________________________________________
% Input pulse generation
% Input pulse is defined as 
% E(t) = A(t)C(t),
% A(t) being the temporal envelope,
% C(t) is the complex frequency component of the carrier wave
A_t = Basics.GenerateTemporalPulseEnvelope(const,InputPulse,time_domain);
C_t = Basics.GenerateTemporalPulseCarrier(const,InputPulse,time_domain);

InputPulse.E_t      = A_t.*C_t;
InputPulse.E_f      = Basics.TemporalToSpectral(InputPulse.E_t);
InputPulse.E_x      = InputPulse.E_f;
InputPulse.I_f      = abs(InputPulse.E_f).^2;
InputPulse.I_t      = abs(InputPulse.E_t).^2;



InputPulse.FWHM_t   = fwhm(time_domain,A_t.^2);
InputPulse.FWHM_f   = fwhm(freq_domain,InputPulse.I_f);
InputPulse.TBP      = InputPulse.FWHM_t * InputPulse.FWHM_f ;

InputPulse;



%__________________________________________________________________________
% Calculating the output pulse
% E(f)_filtered = E(f) * H(f)
% H(f) being the complex filter function
% H(f) = G(f) * P(f)
% G(f) is the real spectral amplitude filter
% P(f) is the complex phase offset filter, where 
% P(f) = cos(p(f)) + i sin(p(f))
% p(f) being the phase addition function.

% STAGE 1 : LC-SLM Pass 1
FilterParams.FilterType = FilterEnums.Phase_Polynomial;
FilterParams.Phase_Polynomial.SixthOrder      = 0 * (const.femto^6);
FilterParams.Phase_Polynomial.FifthOrder      = 0 * (const.femto^5);
FilterParams.Phase_Polynomial.FourthOrder     = 0 * (const.femto^4);
FilterParams.Phase_Polynomial.ThirdOrder      = 0 * (const.femto^3);
FilterParams.Phase_Polynomial.SecondOrder     = 0 * (const.femto^2);
FilterParams.Phase_Polynomial.FirstOrder      = 0 * (const.femto^1);
FilterParams.Phase_Polynomial.ZerothOrder     = 0;
FilterParams.Phase_Polynomial.CenterFrequency = InputPulse.PulseFrequency*const.tera;
[OutputPulse1.E_f,SpectralAmplitudeFilter1,SpectralPhaseFunction1] =   ...
                                FPSpectralFilter.Filtering(const,  ...
                                                           freq_domain,...
                                                           InputPulse, ...
                                                           FilterParams);

       

% STAGE 2 : Amplitude Ramp filter
FilterParams.FilterType = FilterEnums.None;
FilterParams.Amplitude_Ramp.StartWavelength    = 600 * const.nano;  % m
FilterParams.Amplitude_Ramp.EndWavelength      = 1000 * const.nano;    % m
FilterParams.Amplitude_Ramp.Polarity 	= false;
[OutputPulse2.E_f,SpectralAmplitudeFilter2,SpectralPhaseFunction2] =   ...
                                FPSpectralFilter.Filtering(const,  ...
                                                           freq_domain,...
                                                           OutputPulse1, ...
                                                           FilterParams);                      
     

% STAGE 3 : Phase Ramp filter
FilterParams.FilterType = FilterEnums.Phase_Ramp;
FilterParams.Phase_Ramp.CentreWavelength    = 800 * const.nano;      % nm
FilterParams.Phase_Ramp.SpectralGradient = -800*const.femto;
[OutputPulse3.E_f,SpectralAmplitudeFilter3,SpectralPhaseFunction3] =   ...
                                FPSpectralFilter.Filtering(const,  ...
                                                           freq_domain,...
                                                           OutputPulse2, ...
                                                           FilterParams); 


% STAGE 4 : LC-SLM Pass 2
FilterParams.FilterType = FilterEnums.Phase_Polynomial;
[OutputPulse4.E_f,SpectralAmplitudeFilter4,SpectralPhaseFunction4] =   ...
                                FPSpectralFilter.Filtering(const,  ...
                                                           freq_domain,...
                                                           OutputPulse3, ...
                                                           FilterParams); 
        

% STAGE 5: Output of Filter
OutputPulse4.E_t     = Basics.SpectralToTemporal(OutputPulse4.E_f);
OutputPulse4.I_f 	= abs(OutputPulse4.E_f).^2;
OutputPulse4.I_t     = (abs(OutputPulse4.E_t).^2);
OutputPulse4.FWHM_t  = fwhm(time_domain,OutputPulse4.I_t);
OutputPulse4.FWHM_f  = fwhm(freq_domain,OutputPulse4.I_f);
OutputPulse4.TBP     = OutputPulse4.FWHM_t * OutputPulse4.FWHM_f;
OutputPulse4;

NetSpectralPhaseFunction = SpectralPhaseFunction1 + ...
                           SpectralPhaseFunction2 + ... 
                           SpectralPhaseFunction3 + ...
                           SpectralPhaseFunction4;
                       
NetSpectralAmplitudeFilter = SpectralAmplitudeFilter1 .* ...
                             SpectralAmplitudeFilter2 .* ...
                             SpectralAmplitudeFilter3 .* ...
                             SpectralAmplitudeFilter4;
                         
%__________________________________________________________________________
% Plot the results of the Fourier plane filtering

FourierPlanePlotting(const,     ...
                    time_domain,    ...
                    freq_domain,    ...
                    "Frequency",    ...
                    InputPulse,     ...
                    OutputPulse4,    ...
                    NetSpectralAmplitudeFilter, ...
                    NetSpectralPhaseFunction,...
                    cutOff);
                
                % plot histograms of the pulse energy content
                         
PulseEnergy(freq_domain, ...     
            time_domain, ...
            InputPulse, ...    % pulse to be plotted
            InputPulse, ...    % input pulse for reference
            Constants, ...
            1, ...             % frequency bin width (THz)
            5, ...             % time bin width (fs)
            5);              % wavelength bin width (nm)
