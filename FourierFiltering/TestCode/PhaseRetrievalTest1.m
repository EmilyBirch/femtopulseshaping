%__________________________________________________________________________
const = Constants;

%__________________________________________________________________________
% Input Pulse Definition
InputPulse.PulseWidth = 10; % fs
InputPulse.x_dispersion = 0;

InputPulse.Pulse1Wavelength             = 700;  % nm
InputPulse.Pulse1WavelengthWidth        = 20; 	% nm
InputPulse.Pulse1Frequency   = Basics.nmToTHz(const,InputPulse.Pulse1Wavelength);
InputPulse.Pulse1FrequencyWidth = ...
                abs(InputPulse.Pulse1Frequency - ...
                    const.c_nm_to_THz/ ...
                    (InputPulse.Pulse1Wavelength+InputPulse.Pulse1WavelengthWidth));
                
InputPulse.Pulse2Wavelength             = 900;  % nm
InputPulse.Pulse2WavelengthWidth        = 20; 	% nm
InputPulse.Pulse2Frequency   = Basics.nmToTHz(const,InputPulse.Pulse2Wavelength);
InputPulse.Pulse2FrequencyWidth = ...
                abs(InputPulse.Pulse2Frequency - ...
                    const.c_nm_to_THz/ ...
                    (InputPulse.Pulse2Wavelength+InputPulse.Pulse2WavelengthWidth));
                
InputPulse.Pulse3Wavelength             = 800;  % nm
InputPulse.Pulse3WavelengthWidth        = 40; 	% nm
InputPulse.Pulse3Frequency   = Basics.nmToTHz(const,InputPulse.Pulse3Wavelength);
InputPulse.Pulse3FrequencyWidth = ...
                abs(InputPulse.Pulse3Frequency - ...
                    const.c_nm_to_THz/ ...
                    (InputPulse.Pulse3Wavelength+InputPulse.Pulse3WavelengthWidth));


%__________________________________________________________________________
% Generating time,frequency, and spatial domains

% Define time, frequency and Spatial domains
N       = 5000;  % Number of samples in the simulation
cutOff  = ceil(N/2);

[InputPulse,t_increments,time_domain,freq_domain,spatial_domain] = Basics.GenerateTimeDomain(const,InputPulse,N);

%__________________________________________________________________________
% Input pulse generation in spectral domain

A1_f = gaussmf(freq_domain,[InputPulse.Pulse1FrequencyWidth*const.tera ...
                            InputPulse.Pulse1Frequency*const.tera]); 
A2_f = gaussmf(freq_domain,[InputPulse.Pulse2FrequencyWidth*const.tera ...
                            InputPulse.Pulse2Frequency*const.tera]); 
A3_f = 0.5 .* gaussmf(freq_domain,[InputPulse.Pulse3FrequencyWidth*const.tera ...
                            InputPulse.Pulse3Frequency*const.tera]); 
                        
InputPulse.E_f      = A1_f + A2_f + A3_f;


InputPulse.PulseWidthRising = 10; % fs
InputPulse.PulseWidthFalling = 70; % fs
InputPulse.PulseType = InputPulseFunctionEnum.SkewGaussian;
E_t_target = Basics.GenerateTemporalPulseEnvelope(const,InputPulse,time_domain);
[banana] = PhaseRetrieval.GerchbergSaxton(E_t_target, InputPulse.E_f);