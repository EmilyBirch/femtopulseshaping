%__________________________________________________________________________
const = Constants;

InputParams.PulseSeperation =200 * const.femto;

%__________________________________________________________________________
% Calibration

[S1_m,S1_c] = SpatialCalibration.LinearSpatialFit('S1Calibration.csv');
[S2_m,S2_c] = SpatialCalibration.LinearSpatialFit('S2Calibration.csv');
[FM_m,FM_c] = SpatialCalibration.LinearSpatialFit('FMCalibration.csv');

%__________________________________________________________________________
% Laser pulse source parameters
HardwareParams.lambda_zero  = 400 * const.nano;      % Central wavelength of the laser pulse (m)
HardwareParams.f_zero       = Basics.mToHz(const,HardwareParams.lambda_zero);   
                                        % Central frequency of the laser pulse (Hz)

% Fourier Plane parameters
HardwareParams.f            = 0.3;      % focusing mirror focal length (m)
HardwareParams.a            = 1.67e-6;  % diffratction grating period (mm)
HardwareParams.theta_d      = -0.65;    % radians 
HardwareParams.theta_zero   = 1;        % incident angle of laser beam to grating (radians)
HardwareParams.x_dispersion  =  ...
                  (((HardwareParams.lambda_zero)^2)*HardwareParams.f)/...
                  (2*pi*const.c*HardwareParams.a*cos(HardwareParams.theta_d));



% Bi folded mirror parameters                                   
HardwareParams.lambda_mirror = 405 * const.nano;

                                        % Wavelength position of the centre of the folded mirror (m)                     
HardwareParams.FM_f      = Basics.mToHz(const,HardwareParams.lambda_mirror);   
                                        % Frequency position of the centre of the folded mirror (m)
HardwareParams.FMLength     = 0.05;     % Folding mirror length (m)
HardwareParams.s            = ... 
    -1 * FPHardwareFilter.TiltedMirrorDelayAngle(const,HardwareParams,InputParams.PulseSeperation) ;
                                        % Mirror slope (radians)
HardwareParams.FMOffset = sin(HardwareParams.s) * HardwareParams.FMLength;
                                        % Folding Mirror Offset
HardwareParams.FM_x = FPHardwareFilter.FrequencyToSpace(HardwareParams.FM_f,HardwareParams) + FM_c;
                                        
% Slit parameters
% f : frequency position of the center of the slit
% fwidth : total frequency width of the slit
HardwareParams.S1_f         = 720 * const.tera;

HardwareParams.S1_fwidth    = 60 * const.tera;
HardwareParams.S2_f         = 780 * const.tera;
HardwareParams.S2_fwidth    = 60 * const.tera;


% x : position of center of the slit center
% xwidth : total spatial width of the slit
HardwareParams.S1_x         = FPHardwareFilter.FrequencyToSpace(HardwareParams.S1_f,HardwareParams) + S1_c;
HardwareParams.S1_xwidth    = FPHardwareFilter.FrequencyToSpace(HardwareParams.S1_fwidth,HardwareParams);
HardwareParams.S2_x         = FPHardwareFilter.FrequencyToSpace(HardwareParams.S2_f,HardwareParams) + S2_c;
HardwareParams.S2_xwidth    = FPHardwareFilter.FrequencyToSpace(HardwareParams.S2_fwidth,HardwareParams);
    
HardwareParams

%__________________________________________________________________________
% Input Pulse Definition
InputPulse.PulseType        = InputPulseFunctionEnum.Gaussian;
InputPulse.PulseWidth       = 10 * const.femto;                 % s
InputPulse.PulseWavelength  = HardwareParams.lambda_zero ;      % m
InputPulse.PulseFrequency   = HardwareParams.f_zero    ;        % Hz
InputPulse
%__________________________________________________________________________
N       = 10000;  % Number of samples in the simulation
cutOff  = ceil(N/2);
[InputPulse,t_increments,time_domain,freq_domain] = ...
         Basics.GenerateTimeDomain(const,InputPulse,N);  

A_t = Basics.GenerateTemporalPulseEnvelope(const,InputPulse,time_domain);
C_t = Basics.GenerateTemporalPulseCarrier(const,InputPulse,time_domain);

InputPulse.E_t      = A_t.*C_t;
InputPulse.E_f      = Basics.TemporalToSpectral(InputPulse.E_t);
InputPulse.I_t      = abs(InputPulse.E_t).^2;
InputPulse.I_f      = abs(InputPulse.E_f).^2;
        
[OutputPulse.E_f,NetSpectralAmplitudeFilter,NetSpectralPhaseFunction] = ...
        FPHardwareFilter.HardwareFilter(Constants,freq_domain,InputPulse,HardwareParams);


OutputPulse.I_f = abs(OutputPulse.E_f).^2;
OutputPulse.E_t = Basics.SpectralToTemporal(OutputPulse.E_f);
OutputPulse.I_t = abs(OutputPulse.E_t).^2;

figure()
yyaxis left;
plot(freq_domain,InputPulse.I_f,'-r'); hold on;
plot(freq_domain,OutputPulse.I_f,'-g'); hold on;
yyaxis right;
plot(freq_domain,NetSpectralPhaseFunction,'--b'); hold on;
plot(freq_domain,NetSpectralAmplitudeFilter)
X_plot_min = (InputPulse.PulseFrequency - (InputPulse.PulseFrequency));
X_plot_max = (InputPulse.PulseFrequency + (InputPulse.PulseFrequency)); 
xlim([X_plot_min, X_plot_max]);

figure()
plot(time_domain,InputPulse.I_t); hold on;
plot(time_domain,OutputPulse.I_t); hold on;



    