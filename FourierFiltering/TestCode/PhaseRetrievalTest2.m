%__________________________________________________________________________
const= Constants;

%__________________________________________________________________________
% Define Hardware const

Hardware.f       = 30; % focal length (cm)
Hardware.a       = 1.67e-6; % grating period 
Hardware.theta_d = -0.65; % radians 

%__________________________________________________________________________
% Input Pulse Definition
InputPulse.PulseType        = InputPulseFunctionEnum.Gaussian;
InputPulse.PulseWidth       = 10; 	% fs
InputPulse.PulseWavelength  = 800; % nm
InputPulse.PulseFrequency   = ...
                const.c_nm_to_THz/InputPulse.PulseWavelength;  % THz
InputPulse.x_dispersion  =  ...
                  (((InputPulse.PulseWavelength*const.nano)^2)*Hardware.f)/...
                  (2*pi*const.c*Hardware.a*cos(Hardware.theta_d));

%__________________________________________________________________________
% Generating time,frequency, and spatial domains

% Define time, frequency and Spatial domains
N       = 10000;  % Number of samples in the simulation
cutOff  = ceil(N/2);

% Start/end time of simulation containing the pulse centered at 0
InputPulse.Start_time = 50*InputPulse.PulseWidth*const.femto;   

% Increments of time, given the number of samples and length of period
t_increments    = (2 * InputPulse.Start_time)/(N-1);  
time_domain     = -InputPulse.Start_time:t_increments:InputPulse.Start_time;

% Frequency domain conversion 
% Generate array from 0 to N-1 in increments of 1,
% then divide each element by (time_increment * No. Samples)
freq_domain 	= 0:N-1;
freq_domain     = freq_domain/(t_increments * N);  

% Spatial domain conversion
spatial_domain    = freq_domain * InputPulse.x_dispersion;

%__________________________________________________________________________
% Input pulse generation
% Input pulse is defined as 
% E(t) = A(t)C(t),
% A(t) being the temporal envelope,
% C(t) is the complex frequency component of the carrier wave

% Generate the temporal envelope as a Gauss, Sech, or Sinc
switch InputPulse.PulseType
    case InputPulseFunctionEnum.Gaussian
        A_t = gaussmf(time_domain,[InputPulse.PulseWidth*const.femto 0]); 
    case InputPulseFunctionEnum.Sinc
       sinc_width = 50 * const.tera;
       A_t = sinc(time_domain * sinc_width);
    case InputPulseFunctionEnum.Sech
        sech_width = 50 * const.tera;
        A_t = sech(time_domain * sech_width);
end

C_t = exp(2*1i*pi*time_domain *InputPulse.PulseFrequency * const.tera);

InputPulse.E_t      = A_t.*C_t;
InputPulse.E_f      = fft(InputPulse.E_t)/N;
InputPulse.E_x      = InputPulse.E_f;
InputPulse.I_f      = abs(InputPulse.E_f(1:cutOff)).^2;
InputPulse.I_t      = abs(InputPulse.E_t).^2;

InputPulse.FWHM_t   = fwhm(time_domain,A_t.^2);
InputPulse.FWHM_f   = fwhm(freq_domain(1:cutOff),InputPulse.I_f);
InputPulse.TBP      = InputPulse.FWHM_t * InputPulse.FWHM_f ;

InputPulse

%__________________________________________________________________________
% Calculating the output pulse
% E(f)_filtered = E(f) * H(f)
% H(f) being the complex filter function
% H(f) = G(f) * P(f)
% G(f) is the real spectral amplitude filter
% P(f) is the complex phase offset filter, where 
% P(f) = cos(p(f)) + i sin(p(f))
% p(f) being the phase addition function.

% STAGE 3 : Phase Ramp filter
FilterParams.FilterType = FilterEnums.Phase_Ramp;
FilterParams.Phase_Ramp.CentreWavelength    = 800;      % nm
FilterParams.Phase_Ramp.SpectralGradient = -800*const.femto;
[OutputPulse.E_f,SpectralAmplitudeFilter3,SpectralPhaseFunction3] =   ...
                                FPSpectralFilter.Filtering(const,  ...
                                                           freq_domain,...
                                                           InputPulse, ...
                                                           FilterParams);                       
% Output of Filter
OutputPulse.E_t     = ifft(OutputPulse.E_f)*N;
OutputPulse.I_f 	= abs(OutputPulse.E_f(1:cutOff)).^2;
OutputPulse.I_t     = (abs(OutputPulse.E_t).^2);
OutputPulse.FWHM_t  = fwhm(time_domain,OutputPulse.I_t);
OutputPulse.FWHM_f  = fwhm(freq_domain(1:cutOff),OutputPulse.I_f(1:cutOff));
OutputPulse.TBP     = OutputPulse.FWHM_t * OutputPulse.FWHM_f;
OutputPulse

E_t_target = OutputPulse.E_t;
[banana] = PhaseRetrieval.GerchbergSaxton(E_t_target, InputPulse.E_f);

