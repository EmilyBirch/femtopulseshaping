classdef FPSpatialFilter
    
    methods(Static)
        
        % Null Filter Function
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = NullFilter(Constants,x,FilterParams,x_dispersion)
            N = length(x);
            SpatialAmplitudeFilter = 0:N-1;
            
            for n = 1:N-1
                SpatialAmplitudeFilter(n) = 1;
            end
            
            SpatialPhaseFilter = ones(1,N);
            SpatialPhaseFunction = zeros(1,N);
        end

        % Linear Amplitude Filter
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = LinearFilter(Constants,x,FilterParams,x_dispersion)
            StartPosition      = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.EndWavelength    % cm
            EndPosition        = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.StartWavelength  % cm
            N = length(x);
            SpatialAmplitudeFilter = 0:N-1;
            
            if FilterParams.Linear.Polarity == false
                m = 1 / ((EndPosition - StartPosition));
                d = -1 * m * StartPosition;
            else
                m = -1 / ((EndPosition - StartPosition));
                d = 1- (m * StartPosition);
            end
            
            for n = 1:N-1
                if (x(n) > (StartPosition)) && (x(n) < (EndPosition))
                    SpatialAmplitudeFilter(n) = (m*x(n)) + d;
                else
                    SpatialAmplitudeFilter(n) = 0;
                end
            end
            
            SpatialPhaseFilter = ones(1,N);
            SpatialPhaseFunction = zeros(1,N);
        end 
     
        % Polynomial (Up to 6th Order) Phase Filter
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = PolynomialPhaseFilter(Constants,x,FilterParams,x_dispersion)
            N = length(x);
            PhaseFunction = 0:N-1;
            SpatialPhaseFunction = 0:N-1;
            SpatialAmplitudeFilter = ones(1,N);
            SpatialPhaseFunction = (FilterParams.Polynomial.SixthOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^6) + ...
                             FilterParams.Polynomial.FifthOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^5) + ...
                             FilterParams.Polynomial.FourthOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^4) + ...
                             FilterParams.Polynomial.ThirdOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^3) + ...
                             FilterParams.Polynomial.SecondOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^2) + ...
                             FilterParams.Polynomial.FirstOrder*((x-(FilterParams.Polynomial.CenterFrequency) ).^1) + ...
                             FilterParams.Polynomial.ZerothOrder);
            SpatialPhaseFilter = (cos(SpatialPhaseFunction ) + (1i * sin(SpatialPhaseFunction )));    
        end
            
        % Gaussian Amplitude Filter
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = GaussianFilter(Constants,x,FilterParams,x_dispersion)
            CenterPosition = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.Gaussian.CenterWavelength %cm
            SpatialSigma = x_dispersion * Constants.tera * FilterParams.Gaussian.SigmaFrequency %cm 
            N = length(x);
            SpatialAmplitudeFilter = gaussmf(x,   ...
                [(SpatialSigma)  ...
                (CenterPosition)]);
            
            SpatialPhaseFilter = ones(1,N);
            SpatialPhaseFunction = zeros(1,N);
        end
        
        % Box Amplitude Filter
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = BoxFilter(Constants,x,FilterParams,x_dispersion)
            StartPosition      = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.EndWavelength    % cm
            EndPosition        = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.StartWavelength  % cm
            N = length(x);
            SpatialAmplitudeFilter = 0:N-1;
            
            for n = 1:N-1
                if (x(n)>(StartPosition)) && (x(n)<(EndPosition))
                    SpatialAmplitudeFilter(n)=1;
                else
                    SpatialAmplitudeFilter(n)=0;
                end
            end
            
            SpatialPhaseFilter = ones(1,N);
            SpatialPhaseFunction = zeros(1,N);
        end
        
        % Notch Amplitude Filter
        function [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = NotchFilter(Constants,x,FilterParams,x_dispersion)
            StartPosition      = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.EndWavelength    % cm
            EndPosition        = x_dispersion * Constants.c_nm_to_THz * Constants.tera/FilterParams.StartWavelength  % cm
            N = length(x);
            SpatialAmplitudeFilter = 0:N-1;
            
            for n = 1:N-1
                if (x(n)>(StartPosition)) && (x(n)<(EndPosition))
                    SpatialAmplitudeFilter(n)=0;
                else
                    SpatialAmplitudeFilter(n)=1;
                end
            end
            
            SpatialPhaseFilter = ones(1,N);
            SpatialPhaseFunction = zeros(1,N);
        end
        
        % Apply the fourier plane filter to input data.
        function [E_x_filtered,SpatialAmplitudeFilter,SpatialPhaseFunction] = Filtering(Constants,x,InputPulse,FilterParams,x_dispersion)
            switch FilterParams.FilterType
                case FilterEnums.None
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.NullFilter(Constants,x,FilterParams,x_dispersion);
                    
                case FilterEnums.LinearAmplitude
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.LinearFilter(Constants,x,FilterParams,x_dispersion);
                    
                case FilterEnums.PolynomialPhase
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.PolynomialPhaseFilter(Constants,x,FilterParams,x_dispersion);
                    
                case FilterEnums.Gaussian
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.GaussianFilter(Constants,x,FilterParams,x_dispersion);
                    
                case FilterEnums.Box
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.BoxFilter(Constants,x,FilterParams,x_dispersion);
                    
                case FilterEnums.Notch
                    [SpatialAmplitudeFilter,SpatialPhaseFilter,SpatialPhaseFunction] = FPSpatialFilter.NotchFilter(Constants,x,FilterParams,x_dispersion);
                    
                otherwise
                    disp('Invalid Filter')
            end
            
            E_x_filtered = InputPulse.E_f .* SpatialAmplitudeFilter .* SpatialPhaseFilter;
            
        end
        
    end
end
    
    
    
    
