classdef Basics        
    methods(Static)
        function [InputPulse,t_increments,time_domain,freq_domain] = GenerateTimeDomain(const,InputPulse,N)
            % Start/end time of simulation containing the pulse centered at 0
            InputPulse.Start_time = 40*InputPulse.PulseWidth;   

            % Increments of time, given the number of samples and length of period
            t_increments    = (2 * InputPulse.Start_time)/(N-1);  
            time_domain     = -InputPulse.Start_time:t_increments:InputPulse.Start_time;

            % Frequency domain conversion 
            % Generate array from 0 to N-1 in increments of 1,
            % then divide each element by (time_increment * No. Samples)
            freq_domain 	= 0:N-1;
            freq_domain     = freq_domain/(t_increments * N);  

        end
        
        function [A_t] = GenerateTemporalPulseEnvelope(const,InputPulse,time_domain)
           % Generate the temporal envelope as a Gauss, Sech, or Sinc
           N = length(time_domain);
            switch InputPulse.PulseType
                case InputPulseFunctionEnum.Gaussian
                    A_t = gaussmf(time_domain,[InputPulse.PulseWidth 0]); 
                case InputPulseFunctionEnum.Sinc
                   sinc_width = 50 * const.tera;
                   A_t = sinc(time_domain * sinc_width);
                case InputPulseFunctionEnum.Sech
                    sech_width = 50 * const.tera;
                    A_t = sech(time_domain * sech_width);
                case InputPulseFunctionEnum.SkewGaussian
                    A_t(1:(ceil(N/2))) = gaussmf(time_domain(1:ceil((N/2))),[InputPulse.PulseWidthFalling 0]);
                    A_t(floor(N/2):N)   = gaussmf(time_domain(floor(N/2):N),[InputPulse.PulseWidthRising 0]);
            end

        end
        
        function [C_t] = GenerateTemporalPulseCarrier(const,InputPulse,time_domain)
            C_t = exp(2*1i*pi*time_domain *InputPulse.PulseFrequency);
        end
     
        
        function [m] = HzTom(const,Hz)
            m = const.c / Hz;
        end
        
        function [Hz] = mToHz(const,m)
            Hz = const.c / m;
        end
        
        function [E_t] = SpectralToTemporal(E_f)
            N = length(E_f);
            E_t = ifft(E_f)*N;
        end
        
        function [E_f] = TemporalToSpectral(E_t)
            N = length(E_t);
            E_f = fft(E_t)/N;
        end
        
    end
end