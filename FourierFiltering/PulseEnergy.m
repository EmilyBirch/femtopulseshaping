function [freq_energy,freq_domain,wave_domain] = PulseEnergy(freq_domain,time_domain,pulse,InputPulse,Constants,freq_bin,time_bin,wave_bin)
           
          domains = ["freq","time","wave"];
          labels=["Frequency","Frequency (THz)"; "Time","Time (fs)";"Wavelength","Wavelength (nm)"];
          n = length(freq_domain);
          
          domain_arrays=zeros(3,n);
          
          freq_energy=domain_arrays(1,:);
          time_energy=domain_arrays(2,:);
          wave_domain=domain_arrays(3,:);
          
          for n = 1:length(freq_energy)-1         % calculate each dE
              freq_energy(n)=0.5*(pulse.I_f(n+1)+pulse.I_f(n));
              time_energy(n)=0.5*(pulse.I_t(n+1)+pulse.I_t(n));
          end
      
          % ---------- FREQUENCY -----------
          
          freq_domain = freq_domain/Constants.tera;
          
          freq_xmin=(InputPulse.PulseFrequency-4*InputPulse.FWHM_f)/Constants.tera; % generate frequency axis limits for plot
          freq_xmax=(InputPulse.PulseFrequency+4*InputPulse.FWHM_f)/Constants.tera;
          
          % ---------- TIME ------------
      
          time_domain = time_domain/Constants.femto;
          
          time_xmin=min(time_domain); % generate time axis limits for plot
          time_xmax=max(time_domain);
          
          % ---------- WAVELENGTH ------------
           
          for n = 2:length(wave_domain)
                wave_domain(n)= Constants.c_nm_to_THz/(freq_domain(n)); % convert frequency(Hz) into wavelength(nm)
          end
          
          wave_domain(1)=wave_domain(2);
          
          wave_xmin = (0.75*pulse.PulseWavelength)/Constants.nano;
          wave_xmax = (1.25*pulse.PulseWavelength)/Constants.nano;
          
          wave_domain=fliplr(wave_domain);
          
          wave_xmin_index = find(wave_domain>wave_xmin,1);
          wave_xmax_index = find(wave_domain>wave_xmax,1);
         
          wave_domain=wave_domain(wave_xmin_index:wave_xmax_index);
          
          wave_energy=fliplr(freq_energy);
          wave_energy=wave_energy(wave_xmin_index:wave_xmax_index);
         
          % --------- CREATE HISTOGRAMS -----------
          
          for k = 1:length(domains)
              
                energy = eval(domains(k)+"_energy");
                xmin = eval(domains(k)+"_xmin");
                xmax = eval(domains(k)+"_xmax");
                domain = eval(domains(k)+"_domain");
                bin = eval(domains(k)+"_bin");
    
                energy=energy/sum(energy);          % normalise
         
                interpl=linspace(xmin,xmax,10000);
                energy_interpl=interp1(domain,energy,interpl,'spline');
                energy_interpl=energy_interpl/sum(energy_interpl);
           
                index_bin=round((xmax-xmin)/bin);
          
                hist_bins=zeros(1,index_bin);  % empty wavelength histogram array using user-specified bin
                hist_y=zeros(1,length(hist_bins));   % empty histogram y axis
                energy_bin=round(length(energy_interpl)/index_bin);    % energy bin size that we will integrate/sum over
            
                for i=1:length(hist_bins)        % generate histogram x axis using increments of frequency bin
                    j=(i-1)*bin;
                    hist_bins(i)= j;
                end
           
                for i=1:length(hist_y)       % generate wavelength bin energy content by summing dE(lambda) in each bin
                    sum_lower=1+energy_bin*(i-1);
                    sum_higher=-1+energy_bin*i; 
                    if sum_higher>length(energy_interpl)
                        sum_higher=length(energy_interpl);
                    end
                    hist_y(i)=sum(energy_interpl(sum_lower:sum_higher));
                end 
               
                hist_bins=hist_bins+xmin;
                
                %plot histogram
                figure()
                bar(hist_bins,100*hist_y,'BarWidth', 1,'EdgeColor',[0 0 0],'LineWidth',1.5)
                title(labels(k,1))
                ylabel('Energy content (%)')
                xlabel(labels(k,2))
           
          end
end    