classdef Constants
    properties (Constant = true)
        % Define Natural Constants
        femto   = 1e-15;
        nano    = 1e-9;
        tera    = 1e12;
        peta    = 1e15;
        c       = 3e8;
        h       = 6.63e-34;
        % When converting nm into units of THz, so Frequency (THz) = constant / Wavelength (nm)
        c_nm_to_THz = 3e5;
    end
end