function varargout = GUI_Graph(varargin)
% GUI_GRAPH MATLAB code for GUI_Graph.fig
%      GUI_GRAPH, by itself, creates a new GUI_GRAPH or raises the existing
%      singleton*.
%
%      H = GUI_GRAPH returns the handle to a new GUI_GRAPH or the handle to
%      the existing singleton*.
%
%      GUI_GRAPH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_GRAPH.M with the given input arguments.
%
%      GUI_GRAPH('Property','Value',...) creates a new GUI_GRAPH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_Graph_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_Graph_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_Graph

% Last Modified by GUIDE v2.5 19-Oct-2018 13:26:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_Graph_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_Graph_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_Graph is made visible.
function GUI_Graph_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_Graph (see VARARGIN)

% Choose default command line output for GUI_Graph
handles.output = hObject;
handles.filter.linear.polarity = false;

% Update handles structure
guidata(hObject, handles);

set(handles.LinearFilterPanel,'visible','off');
set(handles.GaussianFilterPanel,'visible','off');  


% UIWAIT makes GUI_Graph wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_Graph_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

femto=1e-15;
fs = 1e18; 
nm = 1e-9;
THz = 1e12;
c=3e8;

start_t = 10*handles.pulse_width*femto-1/fs;
t = -start_t:1/(fs):start_t;

A_t=gaussmf(t,[handles.pulse_width*femto 0]);
eb_t = A_t.*sin(2*pi*t *handles.InputFrequency * THz);
N = length(eb_t);


% define the frequency components of the series 
freq = 0:N-1;
freq = freq*fs/N;

% normalise the data and compute the fourier transform
e_w = ifft(eb_t); 
cutOff = ceil(N/2);
e_wplot = e_w(1:cutOff);
freq_plot = freq(1:cutOff);

% filtering here
filter = 0:N-1;

filter_sel_index = get(handles.FilterSelection, 'Value')
switch filter_sel_index
    %__Null Filter__  
    case 1
        for n = 1:N-1
            filter(n) = 1;
        end
        
    %__Linear Filter__    
    case 2
        if handles.filter.linear.polarity == false
            m = 1 / ((handles.filter.linear.endfrequency - handles.filter.linear.startfrequency) * THz);
            d = -1 * m * handles.filter.linear.startfrequency * THz;
        else
            m = -1 / ((handles.filter.linear.endfrequency - handles.filter.linear.startfrequency) * THz);
            d = 1- (m * handles.filter.linear.startfrequency * THz);
        end
        
        for n = 1:N-1
            if (freq(n) > (handles.filter.linear.startfrequency * THz)) && (freq(n) < (handles.filter.linear.endfrequency * THz))
                filter(n) = (m * freq(n) ) + d;
            else
                filter(n) = 0;
            end       
        end
        
    %__Gaussian Filter__
    case 3
        filter = gaussmf(freq,[(handles.filter.gaussian.sigmafrequency* THz) (handles.filter.gaussian.centerfrequency* THz)]); 
end

e_w_filtered = e_w .* filter;
e_w_filtered_plot = e_w_filtered(1:cutOff);


ea_t=fft(e_w_filtered);


axes(handles.axes1);
cla;
plot(freq_plot,smooth(abs(e_wplot),'moving','x'));
xlim([0 ((handles.InputFrequency * THz) + (handles.InputFrequency * THz * 2))]);
xlabel('\omega');
ylabel('E(\omega)');

axes(handles.axes3);
cla;
plot(t, eb_t);
xlim([-start_t start_t]);

axes(handles.axes2)
cla;
plot(freq,filter);
xlim([0 ((handles.InputFrequency * THz) + (handles.InputFrequency * THz * 2))]);

axes(handles.axes5);
cla;
plot(t, real(ea_t));
xlim([-start_t start_t]);

axes(handles.axes4);
cla;
yyaxis left;
ylabel('E(\omega)');
plot(freq_plot,abs(e_w_filtered_plot));
yyaxis right;
ylabel('\phi (\omega)');
plot(freq_plot, angle(e_w_filtered_plot), '--');
xlim([0 ((handles.InputFrequency * THz) + (handles.InputFrequency * THz * 2))]);
xlabel('\omega');


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)



function InputFrequency_editbox_Callback(hObject, eventdata, handles)
% hObject    handle to InputFrequency_editbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of InputFrequency_editbox as text
%        str2double(get(hObject,'String')) returns contents of InputFrequency_editbox as a double
frequency = str2double(get(hObject,'String'));
handles.InputFrequency = frequency;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function InputFrequency_editbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputFrequency_editbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
pulse_width = str2double(get(hObject,'String'));
handles.pulse_width = pulse_width;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in FilterSelection.
function FilterSelection_Callback(hObject, eventdata, handles)
% hObject    handle to FilterSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns FilterSelection contents as cell array
%        contents{get(hObject,'Value')} returns selected item from FilterSelection
filter_sel_index = get(handles.FilterSelection, 'Value');
switch filter_sel_index
    case 1 
        set(handles.LinearFilterPanel,'visible','off');
        set(handles.GaussianFilterPanel,'visible','off');      
    case 2
        set(handles.LinearFilterPanel,'visible','on');
        set(handles.GaussianFilterPanel,'visible','off');
    case 3
        set(handles.LinearFilterPanel,'visible','off');
        set(handles.GaussianFilterPanel,'visible','on');
end



% --- Executes during object creation, after setting all properties.
function FilterSelection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to FilterSelection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'None', 'Linear', 'Gaussian'});



function LinearStartFreqeuncy_Callback(hObject, eventdata, handles)
% hObject    handle to LinearStartFreqeuncy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LinearStartFreqeuncy as text
%        str2double(get(hObject,'String')) returns contents of LinearStartFreqeuncy as a double
frequency = str2double(get(hObject,'String'));
handles.filter.linear.startfrequency = frequency;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function LinearStartFreqeuncy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LinearStartFreqeuncy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function LinearEndFrequency_Callback(hObject, eventdata, handles)
% hObject    handle to LinearEndFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LinearEndFrequency as text
%        str2double(get(hObject,'String')) returns contents of LinearEndFrequency as a double
frequency = str2double(get(hObject,'String'));
handles.filter.linear.endfrequency = frequency;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function LinearEndFrequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LinearEndFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in LinearPolarity.
function LinearPolarity_Callback(hObject, eventdata, handles)
% hObject    handle to LinearPolarity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of LinearPolarity
polarity = get(hObject,'Value');
handles.filter.linear.polarity = polarity;
guidata(hObject,handles);



function Filter_GaussianCenterFrequency_Callback(hObject, eventdata, handles)
% hObject    handle to Filter_GaussianCenterFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Filter_GaussianCenterFrequency as text
%        str2double(get(hObject,'String')) returns contents of Filter_GaussianCenterFrequency as a double
frequency = str2double(get(hObject,'String'));
handles.filter.gaussian.centerfrequency = frequency;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function Filter_GaussianCenterFrequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter_GaussianCenterFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Filter_GaussianSigmaFrequency_Callback(hObject, eventdata, handles)
% hObject    handle to Filter_GaussianSigmaFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Filter_GaussianSigmaFrequency as text
%        str2double(get(hObject,'String')) returns contents of Filter_GaussianSigmaFrequency as a double
frequency = str2double(get(hObject,'String'));
handles.filter.gaussian.sigmafrequency = frequency;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function Filter_GaussianSigmaFrequency_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Filter_GaussianSigmaFrequency (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
