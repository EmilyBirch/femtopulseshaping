classdef FPSpectralFilter
    
    methods(Static)
        
        % Null Filter Function
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = NullFilter(Constants,f,FilterParams)
            N = length(f);
            SpectralAmplitudeFilter = 0:N-1;
            
            for n = 1:N
                SpectralAmplitudeFilter(n) = 1;
            end
            
            SpectralPhaseFilter = ones(1,N);
            SpectralPhaseFunction = zeros(1,N);
        end

        % Linear Amplitude Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = AmplitudeRampFilter(Constants,f,FilterParams)
            StartFrequency      = Basics.mToHz(FilterParams.Amplitude_Ramp.EndWavelength);    % Hz
            EndFrequency        = Basics.mToHz(FilterParams.Amplitude_Ramp.StartWavelength);  % Hz
            N = length(f);
            SpectralAmplitudeFilter = 0:N-1;
            
            if FilterParams.Amplitude_Ramp.Polarity == false
                m = 1 / ((EndFrequency - StartFrequency));
                d = -1 * m * StartFrequency;
            else
                m = -1 / ((EndFrequency - StartFrequency));
                d = 1- (m * StartFrequency);
            end
            
            for n = 1:N
                if (f(n) > (StartFrequency)) && (f(n) < (EndFrequency))
                    SpectralAmplitudeFilter(n) = (m*f(n)) + d;
                else
                    SpectralAmplitudeFilter(n) = 0;
                end
            end
            
            SpectralPhaseFilter = ones(1,N);
            SpectralPhaseFunction = zeros(1,N);
        end 
     
        % Polynomial (Up to 6th Order) Phase Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = PhasePolynomialFilter(Constants,f,FilterParams)
            N = length(f);
            PhaseFunction = 0:N-1;
            SpectralPhaseFunction = 0:N-1;
            SpectralAmplitudeFilter = ones(1,N);
            SpectralPhaseFunction = (FilterParams.Phase_Polynomial.SixthOrder*((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^6) + ...
                             FilterParams.Phase_Polynomial.FifthOrder  *((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^5) + ...
                             FilterParams.Phase_Polynomial.FourthOrder *((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^4) + ...
                             FilterParams.Phase_Polynomial.ThirdOrder  *((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^3) + ...
                             FilterParams.Phase_Polynomial.SecondOrder *((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^2) + ...
                             FilterParams.Phase_Polynomial.FirstOrder  *((f-(FilterParams.Phase_Polynomial.CenterFrequency) ).^1) + ...
                             FilterParams.Phase_Polynomial.ZerothOrder);
            SpectralPhaseFilter = (cos(SpectralPhaseFunction ) + (1i * sin(SpectralPhaseFunction )));    
        end
            
        % Amplitude_Gaussian Amplitude Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = AmplitudeGaussianFilter(Constants,f,FilterParams)
            CenterFrequency = Basics.mToHz(FilterParams.Amplitude_Gaussian.CenterWavelength); % Hz
            N = length(f);
            SpectralAmplitudeFilter = gaussmf(f,   ...
                [(FilterParams.Amplitude_Gaussian.SigmaFrequency)  ...
                (CenterFrequency)]);
            
            SpectralPhaseFilter = ones(1,N);
            SpectralPhaseFunction = zeros(1,N);
        end
        
        % Amplitude_Box Amplitude Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = AmplitudeBoxFilter(Constants,f,FilterParams)
            StartFrequency      = Basics.mToHz(Constants,FilterParams.Amplitude_Box.EndWavelength) ;   % Hz
            EndFrequency        = Basics.mToHz(Constants,FilterParams.Amplitude_Box.StartWavelength);  % Hz
            N = length(f);
            SpectralAmplitudeFilter = 0:N-1;
            
            for n = 1:N
                if (f(n)>(StartFrequency)) && (f(n)<(EndFrequency))
                    SpectralAmplitudeFilter(n)=1;
                else
                    SpectralAmplitudeFilter(n)=0;
                end
            end
            
            SpectralPhaseFilter = ones(1,N);
            SpectralPhaseFunction = zeros(1,N);
        end
        
        % Amplitude_Notch Amplitude Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = AmplitudeNotchFilter(Constants,f,FilterParams)
            StartFrequency      = Basics.mToHz(Constants,FilterParams.Amplitude_Notch.EndWavelength);    % Hz
            EndFrequency        = Basics.mToHz(Constants,FilterParams.Amplitude_Notch.StartWavelength);  % Hz
            N = length(f);
            SpectralAmplitudeFilter = 0:N-1;
            
            for n = 1:N
                if (f(n)>=(StartFrequency)) && (f(n)<=(EndFrequency))
                    SpectralAmplitudeFilter(n)=0;
                else
                    SpectralAmplitudeFilter(n)=1;
                end
            end
            
            SpectralPhaseFilter = ones(1,N);
            SpectralPhaseFunction = zeros(1,N);
        end
        
        % Phase Triangle Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = PhaseTriangleFilter(Constants,f,FilterParams);
            CentreFrequency     = FilterParams.Phase_Triangle.CentreFrequency ;  % Hz
            StartFrequency      = f(1);
            EndFrequency        = 2 * ((CentreFrequency) - f(1));
            Gradient = FilterParams.Phase_Triangle.SpectralGradient;
            
            N = length(f);        
            SpectralAmplitudeFilter = ones(1,N);
            SpectralPhaseFunction = 0:N-1;
            
            c_1 = -1 * (Gradient * (StartFrequency));
            c_2 = (Gradient * (EndFrequency));

            for n = 1:N
               if (f(n)<(CentreFrequency)) 
                   SpectralPhaseFunction(n) = (Gradient * f(n))  + c_1;
                   
               elseif (f(n)>(CentreFrequency) && f(n)<EndFrequency)
                   SpectralPhaseFunction(n) = (-1 * Gradient * f(n))  + c_2;
                
               else
                   SpectralPhaseFunction(n) = 0;
               end
            end
            
            SpectralPhaseFilter = (cos(SpectralPhaseFunction ) + (1i * sin(SpectralPhaseFunction ))); 
        end
        
        % Phase Ramp Filter
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = PhaseRampFilter(Constants,f,FilterParams)
            CentreFrequency     = Basics.mToHz(Constants,FilterParams.Phase_Ramp.CentreWavelength); % Hz
            Gradient            = FilterParams.Phase_Ramp.SpectralGradient; % AU, +ve
            N = length(f);        
            SpectralAmplitudeFilter = ones(1,N);
            SpectralPhaseFunction = 0:N-1;
            c_1 = -1 * (Gradient * (CentreFrequency));
            
            for n = 1:N
               if (f(n)<=(CentreFrequency))
                   SpectralPhaseFunction(n) = 0;
               elseif (f(n)>(CentreFrequency))
                   SpectralPhaseFunction(n) = (Gradient * f(n))  + c_1;
               end
            end

            SpectralPhaseFilter = (cos(SpectralPhaseFunction ) + (1i * sin(SpectralPhaseFunction ))); 
        end      
        
        % Apply the fourier plane filter to input data.
        function [E_f_filtered,SpectralAmplitudeFilter,SpectralPhaseFunction] = Filtering(Constants,f,InputPulse,FilterParams)
            switch FilterParams.FilterType
                case FilterEnums.None
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.NullFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Amplitude_Ramp
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.AmplitudeRampFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Phase_Polynomial
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.PhasePolynomialFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Amplitude_Gaussian
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.Amplitude_GaussianFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Amplitude_Box
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.AmplitudeBoxFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Amplitude_Notch
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.AmplitudeNotchFilter(Constants,f,FilterParams);
                    
                case FilterEnums.Phase_Triangle
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.PhaseTriangleFilter(Constants,f,FilterParams);                    
            
                case FilterEnums.Phase_Ramp
                    [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = FPSpectralFilter.PhaseRampFilter(Constants,f,FilterParams);                    
                    
                otherwise
                    disp('Invalid Filter')
            end
            
            E_f_filtered = InputPulse.E_f .* SpectralAmplitudeFilter .* SpectralPhaseFilter;
            
        end
        
    end
end
    
    
    
    
