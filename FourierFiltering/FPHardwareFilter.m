classdef FPHardwareFilter
    
    methods(Static)
        function [space] = FrequencyToSpace(frequency,HardwareParams)
           space = frequency *  HardwareParams.x_dispersion;
        end
        
        function [frequency] = SpaceToFrequency(space,HardwareParams)
            frequency = space / HardwareParams.x_dispersion;
        end
        
        function [gradient] = TiltedMirrorGradient(const,HardwareParams)
            gradient = (4 * pi * HardwareParams.f * HardwareParams.s) / ...
                       (HardwareParams.a * cos(HardwareParams.theta_zero) * HardwareParams.f_zero);
        end       
                   
        % Input a desired delay in fs for the two pulses, returns the
        % required mirror offset in radians. 
        % Positive time has high frequency leading, low frequency lagging
        function [MirrorAngle] = TiltedMirrorDelayAngle(const,HardwareParams,delay)
            MirrorAngle = delay * (HardwareParams.f_zero * HardwareParams.a * cos(HardwareParams.theta_zero)) / ...
                            (4 * HardwareParams.f);
        end
            
        function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = ...
                    TiltedMirrors(const,freq_domain,HardwareParams)
                
            N = length(freq_domain);
            
            gradient = FPHardwareFilter.TiltedMirrorGradient(const,HardwareParams);           
            

            FilterParams.Phase_Triangle.CentreFrequency = HardwareParams.FM_f;

            FilterParams.Phase_Triangle.SpectralGradient = gradient;
                   
            [MirrorSpectralAmplitudeFilter,MirrorSpectralPhaseFilter,MirrorSpectralPhaseFunction] = ...
                    FPSpectralFilter.PhaseTriangleFilter(const,freq_domain,FilterParams);
                
            mirror_gap = abs(200*HardwareParams.FMLength * (1 - cos(HardwareParams.s)))
                    % size of gap between mirrors (m)
                
            frequency_gap = FPHardwareFilter.SpaceToFrequency(mirror_gap,HardwareParams);    

            gap_f1 = HardwareParams.FM_f - (frequency_gap /2);
            gap_f2 = HardwareParams.FM_f + (frequency_gap /2);

            
            FilterParams.Amplitude_Notch.EndWavelength = Basics.HzTom(const,gap_f2);
            FilterParams.Amplitude_Notch.StartWavelength = Basics.HzTom(const,gap_f1);
            
            [GapSpectralAmplitudeFilter,GapSpectralPhaseFilter,GapSpectralPhaseFunction] = ...
                    FPSpectralFilter.AmplitudeNotchFilter(const,freq_domain,FilterParams);
            
            SpectralPhaseFunction =  MirrorSpectralPhaseFunction + GapSpectralPhaseFunction;
            SpectralAmplitudeFilter =  MirrorSpectralAmplitudeFilter .* GapSpectralAmplitudeFilter; 
            SpectralPhaseFilter =  MirrorSpectralPhaseFilter .* GapSpectralPhaseFilter;  

       end 
       
       function [SpectralAmplitudeFilter,SpectralPhaseFilter,SpectralPhaseFunction] = ...
                    Slits(const,freq_domain,HardwareParams)
           N = length(freq_domain);
           
           FilterParams1.Amplitude_Box.EndWavelength = ...
                        Basics.HzTom(const,HardwareParams.S1_f - (HardwareParams.S1_fwidth/2));
           FilterParams1.Amplitude_Box.StartWavelength = ...
                        Basics.HzTom(const,HardwareParams.S1_f + (HardwareParams.S1_fwidth/2));
           FilterParams2.Amplitude_Box.EndWavelength = ...
                        Basics.HzTom(const,HardwareParams.S2_f - (HardwareParams.S2_fwidth/2));
           FilterParams2.Amplitude_Box.StartWavelength = ...
                        Basics.HzTom(const,HardwareParams.S2_f + (HardwareParams.S2_fwidth/2));
           
           [S1SpectralAmplitudeFilter,S1SpectralPhaseFilter,S1SpectralPhaseFunction] = ...
                            FPSpectralFilter.AmplitudeBoxFilter(Constants,freq_domain,FilterParams1);

           [S2SpectralAmplitudeFilter,S2SpectralPhaseFilter,S2SpectralPhaseFunction] = ...
                            FPSpectralFilter.AmplitudeBoxFilter(Constants,freq_domain,FilterParams2);
                        
           SpectralAmplitudeFilter = S1SpectralAmplitudeFilter + S2SpectralAmplitudeFilter;
           SpectralPhaseFunction = S1SpectralPhaseFunction + S2SpectralPhaseFunction;
           SpectralPhaseFilter = S1SpectralPhaseFilter .* S2SpectralPhaseFilter;
           
           figure()
           plot(freq_domain,SpectralAmplitudeFilter)
       end
        
       function [E_f_output,NetSpectralAmplitudeFilter,NetSpectralPhaseFunction] = ...
                    HardwareFilter(const,freq_domain,InputPulse,HardwareParams)
       
            [TiltedMirrorAmplitudeFilter,TiltedMirrorPhaseFilter,TiltedMirrorPhaseFunction] = ...
                    FPHardwareFilter.TiltedMirrors(const,freq_domain,HardwareParams);
           
            [SlitsAmplitudeFilter,SlitsPhaseFilter,SlitsPhaseFunction] = ...
                    FPHardwareFilter.Slits(const,freq_domain,HardwareParams);
                
            NetSpectralAmplitudeFilter = TiltedMirrorAmplitudeFilter .* SlitsAmplitudeFilter;
            NetSpectralPhaseFilter     = TiltedMirrorPhaseFilter .* SlitsPhaseFilter;
            NetSpectralPhaseFunction   = TiltedMirrorPhaseFunction + SlitsPhaseFunction;
            
            E_f_output = InputPulse.E_f .* NetSpectralAmplitudeFilter .* NetSpectralPhaseFilter;

       end
    end
end